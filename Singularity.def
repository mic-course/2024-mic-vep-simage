BootStrap: docker
From: ensemblorg/ensembl-vep

# ===========
%labels
  Maintainer DCI Bioinformatics
  bcftools 1.19
  samtools 1.19.2
  htslib 1.19.1
  vcf2maf 1.6.21

%environment
  export PATH=/usr/local/bin:${PATH}:/scif/apps/bcftools/bin
  export PATH=/usr/local/bin:${PATH}:/scif/apps/samtools/bin

%files
  # vcf2maf.pl is the original vcf2maf script pulled from the vcf2maf Github 
  # repo. vcf2maf_mod.pl is a slightly modified version that has a single string
  # (--af_esp removed from line 465) removed to fix a bug. This fix was 
  # suggested in this Github issue thread: https://github.com/mskcc/vcf2maf/issues/325
  scripts/vcf2maf_mod.pl

%post
  export DEBIAN_FRONTEND=noninteractive

  # set env variables for build
  export BCFTOOLS_VERSION=1.19
  export SAMTOOLS_VERSION=1.19.2
  export VCF2MAF_VERSION=1.6.21
  export WGET="wget --progress=bar:force:noscroll --inet4-only"

  apt-get update 
  apt-get --assume-yes --no-install-recommends install wget libncurses5-dev

  apt-get -qq install -y --no-install-recommends tzdata apt-utils
  ln -fs /usr/share/zoneinfo/America/New_York /etc/localtime
  dpkg-reconfigure --frontend noninteractive  tzdata

  # Configure default locale
  echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
  locale-gen en_US.utf8
  /usr/sbin/update-locale LANG=en_US.UTF-8
  export LC_ALL=en_US.UTF-8
  export LANG=en_US.UTF-8
  export TZ=UTC

  # clean
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*

  unset DEBIAN_FRONTEND

# vcf2maf
cd /opt/ && \
wget https://github.com/mskcc/vcf2maf/archive/refs/tags/v${VCF2MAF_VERSION}.tar.gz && \
tar -xzf v${VCF2MAF_VERSION}.tar.gz && \
rm v${VCF2MAF_VERSION}.tar.gz

# samtools
%appinstall samtools
  wget https://github.com/samtools/samtools/releases/download/${SAMTOOLS_VERSION}/samtools-${SAMTOOLS_VERSION}.tar.bz2 && \
  tar -jxf samtools-${SAMTOOLS_VERSION}.tar.bz2 && \
  rm samtools-${SAMTOOLS_VERSION}.tar.bz2 && \
  cd samtools-${SAMTOOLS_VERSION} && \
  ./configure && \
  make && \
  ln -s /scif/apps/samtools/samtools-${SAMTOOLS_VERSION}/samtools /scif/apps/samtools/bin/samtools

# bcftools
%appinstall bcftools
  wget https://github.com/samtools/bcftools/releases/download/${BCFTOOLS_VERSION}/bcftools-${BCFTOOLS_VERSION}.tar.bz2 && \
  tar -jxf bcftools-${BCFTOOLS_VERSION}.tar.bz2 && \
  rm bcftools-${BCFTOOLS_VERSION}.tar.bz2 && \
  cd bcftools-${BCFTOOLS_VERSION} && \
  ./configure && \
  make && \
  ln -s /scif/apps/bcftools/bcftools-${BCFTOOLS_VERSION}/bcftools /scif/apps/bcftools/bin/bcftools


