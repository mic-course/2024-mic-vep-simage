# 2023 MIC Rstudio Singularity Image

Singularity image for 2023 MIC Course

## Notes

### Building Image
```
export IMAGEDIR=/work/${USER}/images
mkdir -p $IMAGEDIR
srun -A chsi -p chsi -c10 --mem 20G apptainer build ${IMAGEDIR}/2023mic.sif Singularity.def
```

### Pulling Image
Below **TAG_NAME** should be replaced with the image tag you want to pull, for example, **latest** or **v0001**

#### Pulling for Production
Pull production images with the following, 

```
singularity pull --force --dir /opt/apps/community/od_chsi_rstudio oras://gitlab-registry.oit.duke.edu/mic-course/2023-mic-singularity-image:TAG_NAME
```

#### Pulling for Development
Pull production images with the following, 

```
export IMAGEDIR=/work/${USER}/images
mkdir -p $IMAGEDIR
singularity pull --force --dir $IMAGEDIR oras://gitlab-registry.oit.duke.edu/mic-course/2023-mic-singularity-image:TAG_NAME
```
