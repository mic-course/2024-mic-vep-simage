---
title: "Tool Test"
author: Tyler Schappe
date: 4/28/22
output: 
  html_document:
    keep_md: TRUE
---

**Goal:** The goal of this file is simply to test whether certain complex/large tools are available in the container. 

## R Packages

```{r}
library(tidyverse)
```

```{r}
library(dada2)
```

```{r}
library(phyloseq)
```



## Bash

```{bash}
fastq-dump
```



