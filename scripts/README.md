# vcf2maf

`vcf2maf.pl` is the original vcf2maf script pulled from their Github repo. `vcf2maf_mod.pl` is a slightly modified version that has a single string (`--af_esp` removed from line 465) removed to fix a bug. This fix was suggested in [this Github issue thread](https://github.com/mskcc/vcf2maf/issues/325).

This issue still exists as of 2/7/2024; there are a few other Github issues that note that the `--af_esp` flag is deprecated and is the reason why it causes errors:

- https://github.com/mskcc/vcf2maf/pull/324
- https://github.com/mskcc/vcf2maf/issues/322
